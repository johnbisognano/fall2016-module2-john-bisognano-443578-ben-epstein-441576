<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home for files</title>
    <link rel='stylesheet' type='text/css' href='style_home.css'/>
</head>
<body>

<?php
session_start();
if(!isset($_SESSION['user'])){     //for safety, so you cant change link on website and see files
    header("Location: FileShare.html");
}
?>

<form  enctype="multipart/form-data" action="file.php" method="POST">

    <div id = logout><button class="btn" type="submit" name="action" value="LogOut">Log Out</button><br><br></div>
    <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
    <div id = choose><label for="uploadfile_input">Choose a file to upload:</label>
        <input name="uploadedFile" type="file" id="uploadfile_input">
        <br></div>
    <div id = upload><button class="btn" type="submit" name="action" value="Upload">Upload</button>
        <br><br></div>



    <div id="files">
        <?php
    //session_start();
    $curUser = $_SESSION['user'];
    $files = scandir( "/srv/uploads/$curUser" );  //displaying files that have been uploaded (srv/uploads/username)
    foreach( $files as $file ) {
        if($file != "." && $file!="..") {
            echo "<label for='$file'>" . $file . "</label>";
            echo "<input type='radio' name= 'file' id='$file' value='$file'> </input>" . "<br />";

        }
    }
    ?></div>
    <div id = delete><button class="btn" type="submit" name="action" value="Delete">Delete</button></div>
    <div id = download><button class="btn" type="submit" name="action" value="Download">Download</button><br><br></div>
    <div id = share><button class="btn" type="submit" name="action" value="Share">Share</button>
        <label for="Share"> Share with a friend ;)</label>
        <input type="text" name="share" id="Share"/><br><br></div>
    <!--    <label for="userDel"> Delete your account?</label><br>  didn't use this method-->
    <!--    <input type="submit" name="action" value="DeleteUser" id="userDel"/>-->
    <div id = make><label for="userMake"> Make a new user: Enter User Name</label><br>
        <input type="text" name="make"/> <button class="btn" type="submit" name="action" value="MakeUser" id="userMake">Create</button></div>
</form>







</body>
</html>